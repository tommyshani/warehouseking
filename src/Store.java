import com.sun.xml.internal.bind.v2.util.QNameMap;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.HashMap;
import java.lang.Integer;

public class Store {

    private String _name;
    private String _openingHours;
    private double _incomeCounter;
    private int _productCounter;
    private String _address;
    private HashMap<Product, Integer> _inventoryMap;
    private List<Product> _productList;
    private HashMap<String, List<Product>> _purchaseMap;
    private HashMap<String, Integer> _incomeMap;
    private HashMap<Product, Integer> _originalInventoryMap;
    private int _howManyPurchased;

    public Store () {

    }

    public Store (String name, LinkedHashMap<Product, Integer> inventoryMap, String openingHours, String address) {
        _productList = new ArrayList<>();
        _inventoryMap = new HashMap<>();
        _inventoryMap = inventoryMap;
        _purchaseMap = new HashMap<>();
        _incomeMap = new HashMap<>();
        _originalInventoryMap = new HashMap<>();
        _originalInventoryMap = inventoryMap;
        _name = name;
        _openingHours = openingHours;
        _address = address;
    }

    public String getYearMonthDay() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 1);
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        String formatted = format1.format(cal.getTime());
        return formatted;
    }

    public void purchase (Product product, int numTimes) {
        _howManyPurchased = numTimes;
        if (_incomeCounter < 10000) {
            _inventoryMap.put(product, _inventoryMap.get(product) - numTimes);
            if (_inventoryMap.get(product) <= 0) {
                restockProduct(product);
            }
            _incomeCounter += product.getPrice() * numTimes;
            _productCounter += numTimes;
            purchasePurchase(product);
        }
    }

    public void purchasePurchase(Product product) {
        _productList.add(product);
        _purchaseMap.put(getYearMonthDay(), _productList);
    }

    public void restockProduct (Product product) {
        int temp = _inventoryMap.get(product);
        _inventoryMap.put(product, _originalInventoryMap.get(product) - ((temp)*(-1)));
    }

    public int getProductCount(Product product) {
        return _inventoryMap.get(product);
    }

    public String getOpeningHours() {
        return _openingHours;
    }

    public String getName() {
        return _name;
    }

    public int getTotalPurchaseCount(String date) {
        return _purchaseMap.get(date).size();
    }

    public double getTotalIncome (String date) {
        double counter = 0;
        List<Product> products = _purchaseMap.get(date);
        for (Product product : products) {
            counter += _howManyPurchased * product.getPrice();
        }
        return counter;
    }

    public double getIncome () {
        return _incomeCounter;
    }

    public HashMap<Product, Integer> getInventoryMap() {
        return _inventoryMap;
    }

    public HashMap<String, List<Product>> getPurchaseMap() {
        return _purchaseMap;
    }

    public HashMap<Product, Integer> getOriginalInventoryMap() {
        return _originalInventoryMap;
    }

}
