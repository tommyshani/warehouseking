import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedHashMap;

public class Main {

    public static void main(String[] args) {

        Warehouse telAviv = new Warehouse();
        //telAviv.areStoresOpen();

        //STORE 1
        
        Product potato = new Product ("Potato", 5);
        Product cheese = new Product ("Cheese", 3);
        Product meat = new Product ("Meat", 10);

        LinkedHashMap<Product, Integer> inventory = new LinkedHashMap<>();
        inventory.put(potato, 5);
        inventory.put(cheese, 7);
        inventory.put(meat, 8);

        Store ramatAviv = new Store("Ramat Aviv", inventory, "9-5", "Drezner 45");
        telAviv.getStoreList().add(ramatAviv);

        ramatAviv.purchase(potato, 3);
        ramatAviv.purchase(cheese, 2);
        ramatAviv.purchase(meat, 4);

        System.out.println("Today's date is " + ramatAviv.getYearMonthDay());
        System.out.println("Ramat Aviv Store: ");
        System.out.println("Our opening hours are " + ramatAviv.getOpeningHours());
        System.out.println("Inventory count of product 1 = " + ramatAviv.getProductCount(potato));
        System.out.println("Inventory count of product 2 = " + ramatAviv.getProductCount(cheese));
        System.out.println("Inventory count of product 3 = " + ramatAviv.getProductCount(meat));
        System.out.println("Current income is = $" + ramatAviv.getIncome());
        System.out.println("Today's purchase count is " + ramatAviv.getTotalPurchaseCount("2017-01-06"));
        System.out.println("Today's total income is $" + ramatAviv.getTotalIncome("2017-01-06"));


        /**

        //STORE 2
        
        Product beef = new Product ("Beef", 10);
        Product chicken = new Product ("Chicken", 7);

        Store ramatHasharon = new Store("Ramat HaSharon", beef, potato, chicken,
                400, 255, 620, "9-5", "Sokolov 95");

        ramatHasharon.purchase(beef, 230);
        ramatHasharon.purchase(potato, 98);
        ramatHasharon.purchase(chicken, 170);

        System.out.println("Ramat Hasharon Store:");
        System.out.println("Our opening hours are " + ramatHasharon.getOpeningHours());
        System.out.println("Inventory count of product 1 = " + ramatHasharon.getProductCount(beef));
        System.out.println("Inventory count of product 2 = " + ramatHasharon.getProductCount(potato));
        System.out.println("Inventory count of product 3 = " + ramatHasharon.getProductCount(chicken));
        System.out.println("Current income is = $" + ramatHasharon.getIncome());
        System.out.println("Total products sold today is: " + ramatHasharon.getTotalProductCount());


        System.out.println();

        //STORE 3
        
        Product pasta = new Product ("Pasta", 3);
        Product chocolate = new Product ("Chocolate", 6);
        Product rice = new Product ("Rice", 2);

        Store herziliya = new Store("Herziliya", pasta, chocolate, rice,
                70, 120, 85, "9-5", "Shalva 36");

        herziliya.purchase(pasta, 20);
        herziliya.purchase(chocolate, 45);
        herziliya.purchase(rice, 30);

        System.out.println("Herziliya Store:");
        System.out.println("Our opening hours are " + herziliya.getOpeningHours());
        System.out.println("Inventory count of product 1 = " + herziliya.getProductCount(pasta));
        System.out.println("Inventory count of product 2 = " + herziliya.getProductCount(chocolate));
        System.out.println("Inventory count of product 3 = " + herziliya.getProductCount(rice));
        System.out.println("Current income is = $" + herziliya.getIncome());
        System.out.println("Total products sold today is: " + herziliya.getTotalProductCount());

        System.out.println();

        telAviv.getStoreList().add(ramatAviv);
        telAviv.getStoreList().add(ramatHasharon);
        telAviv.getStoreList().add(herziliya);
        System.out.println("The most successful store is " + telAviv.getMostSuccessful(telAviv.getStoreList()));

         **/
    }

}
