import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Warehouse {

    private List<Store> _storeList;
    private Date _timeAndDate;
    private Store temp;

    public Warehouse () {
        _timeAndDate = new Date();
        _storeList = new ArrayList<>();
    }

    public boolean areStoresOpen() {
        if (_timeAndDate.getHours() < 17 && _timeAndDate.getHours() > 9) {
            return true;
        } else {
            closeStores();
            return false;
        }
    }


    public void closeStores() {
        System.out.println("Sorry, we are closed!");
        System.exit(0);
    }

    public String getMostSuccessful(List<Store> listOfStores) {
        _storeList = listOfStores;
        for (int i = 0; i < listOfStores.size()-1; i++) {
            if (listOfStores.get(i).getIncome() > listOfStores.get(i+1).getIncome()) {
                temp = listOfStores.get(i);
            }
        }
        return temp.getName();
    }

    //public int getProductCountPerDay (Store store, String date) {

    //}

    public Date getDateAndTime () {
        return _timeAndDate;
    }

    public List<Store> getStoreList (){
        return _storeList;
    }

}
