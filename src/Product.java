public class Product {

    private double _price;
    private String _productName;

    public Product () {

    }

    public Product (String productName) {
        _productName = productName;
    }

    public Product (String productName, int price) {
        _productName = productName;
        _price = price;
    }

    public String getProductName() {
        return _productName;
    }
    
    public double getPrice () {
        return _price;
    }

    public void setProductPrice(int newPrice) {
        _price = newPrice;
    }

}
